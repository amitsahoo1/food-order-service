ALTER DATABASE FOODORDERINGSYSTEM SET SINGLE_USER WITH ROLLBACK IMMEDIATE
GO

USE MASTER
GO

IF EXISTS (SELECT * FROM Sys.databases where name='FoodOrderingSystem') DROP DATABASE FoodOrderingSystem
GO

CREATE DATABASE FoodOrderingSystem
GO

USE FoodOrderingSystem
GO



CREATE USER RentalManager FOR LOGIN RentalManager WITH DEFAULT_SCHEMA=dbo
GO
ALTER ROLE db_owner ADD MEMBER RentalManager
GO


CREATE TABLE dbo.Locations(
	LocationId int PRIMARY KEY IDENTITY(1,1) NOT NULL,
	LocationName varchar(50) NULL)
GO

CREATE TABLE dbo.Restaurants(
	RestaurantId int PRIMARY KEY IDENTITY(1,1),
	RestaurantName varchar(20) NOT NULL,
	RestaurantIcon varchar(max) NOT NULL,
	LocationId int NOT NULL,
	Rating int NOT NULL,
	RatingCount int NOT NULL,
	CONSTRAINT FK_Resturant_Location FOREIGN KEY(LocationId) REFERENCES Locations (LocationId)
	)
GO

CREATE TABLE dbo.Addresses(
	AddressId int PRIMARY KEY IDENTITY(1,1),
	AddressLine1 varchar(50) NOT NULL,
	AddressLine2 varchar(50) NULL,
	States varchar(50) NOT NULL,
	Pincode int NOT NULL,
	RestaurantId int NOT NULL,
	CONSTRAINT FK_Address_Restutant FOREIGN KEY(RestaurantId) REFERENCES Restaurants (RestaurantId),
)
GO

CREATE TABLE dbo.Menus(
	MenuId int PRIMARY KEY IDENTITY(1,1) NOT NULL,
	ItemName varchar(30) NOT NULL,
	ItemDescription varchar(200) NOT NULL,
	ItemIcon varchar(max) not null,
	ItemPrice decimal(6, 2) NOT NULL,
	RestaurantId int NOT NULL,
	CONSTRAINT FK_Menu_Restaurant FOREIGN KEY(RestaurantId) REFERENCES Restaurants (RestaurantId)
	)
GO

CREATE TABLE OrderStatus (
	OrderStatusId int identity(1,1) primary key,
	OrderStatusName varchar(20) not null
)
GO

CREATE TABLE dbo.Orders(
	OrderId int PRIMARY KEY IDENTITY(1,1),
	OrderAmount decimal(8, 2) NOT NULL,
	RestaurantId int NOT NULL,
	OrderStatusId int not null,
	OrderCreatedTime datetime not null,
	OrderModifiedTime datetime,
	CONSTRAINT FK_Orders_Resturant FOREIGN KEY(RestaurantId) REFERENCES dbo.Restaurants (RestaurantId),
	CONSTRAINT FK_Orders_Status FOREIGN KEY(OrderStatusId) REFERENCES dbo.OrderStatus (OrderStatusId)
	)
GO

CREATE TABLE dbo.OrderItem(
	OrderItemId int PRIMARY KEY IDENTITY(1,1),
	ItemId int NOT NULL,
	Quantity int NOT NULL,
	OrderId int NOT NULL
	CONSTRAINT FK_OrderItemOrder FOREIGN KEY(OrderId) REFERENCES Orders (OrderId)
)
GO

CREATE TABLE Discount (
	RestaurantId int,
	Q1 int default 0,
	Q2 int default 0,
	Q3 int default 0,
	Q4 int default 0,
	CONSTRAINT FK_Discount_Restaurant foreign key(RestaurantId) references Restaurants(RestaurantId)
)

