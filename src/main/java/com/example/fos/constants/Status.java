package com.example.fos.constants;

import com.example.fos.model.OrderStatus;

public class Status {
    public static OrderStatus ORDER_PLACED = new OrderStatus(1,"OrderPlaced");
    public static OrderStatus ORDER_ACCEPTED = new OrderStatus(2,"OrderAccepted");
    public static OrderStatus ORDER_CANCELLED = new OrderStatus(3,"OrderCancelled");
    public static OrderStatus ORDER_DELIVERED = new OrderStatus(4,"OrderDelivered");
    public static OrderStatus ORDER_ON_THE_WAY = new OrderStatus(5,"OrderOnTheWay");
}
