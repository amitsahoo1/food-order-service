package com.example.fos.controller;

import com.example.fos.model.Order;
import com.example.fos.model.dto.OrderDto;
import com.example.fos.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
@CrossOrigin("*")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("createOrder")
    public ResponseEntity<?> createOrder(@RequestBody OrderDto order) {
        try{
            Order placedOrder = orderService.createOrder(order);
            return new ResponseEntity<>(placedOrder, HttpStatus.OK);
        } catch (RuntimeException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
