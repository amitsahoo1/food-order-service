package com.example.fos.controller;

import com.example.fos.model.Restaurant;
import com.example.fos.model.dto.RestaurantMenuDto;
import com.example.fos.service.RestaurantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/restaurant")
@CrossOrigin("*")
@Slf4j
public class RestaurantController {

    @Autowired
    private RestaurantService restaurantService;

    @GetMapping("/location")
    public ResponseEntity<?> getRestaurantsByLocations(@RequestParam("loc") String value) {
        try {
            List<Restaurant> result = restaurantService.getRestaurantsByLocations(value);
            return new ResponseEntity<List<Restaurant>>(result, HttpStatus.OK);
        } catch (RuntimeException e) {
            log.error("Error occurred while fetching data for : " + e.getMessage());
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("Error occurred while fetching data for : " + e.getMessage());
            return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping
    public ResponseEntity<?> getRestaurantMenu(@RequestParam("restaurantId") int restaurantId) {
        try {
            RestaurantMenuDto menu = restaurantService.getRestaurantMenu(restaurantId);
            return new ResponseEntity<RestaurantMenuDto>(menu, HttpStatus.OK);
        } catch (RuntimeException e) {
            log.error("Error occurred while fetching data for : " + e.getMessage());
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("Error occurred while fetching data for : " + e.getMessage());
            return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
