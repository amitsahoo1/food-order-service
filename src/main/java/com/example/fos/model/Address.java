package com.example.fos.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "Addresses")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AddressId")
    private int addressId;
    @Column(name = "AddressLine1", length = 50)
    private String addressLine1;
    @Column(name = "AddressLine2", length = 50)
    private String addressLine2;
    @Column(name = "States", length = 50)
    private String states;
    @Column(name = "Pincode")
    private int pincode;

    @OneToOne
    @JoinColumn(name = "restaurantId")
    private Restaurant restaurant;
}
