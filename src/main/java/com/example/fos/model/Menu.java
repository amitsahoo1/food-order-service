package com.example.fos.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "Menus")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MenuId")
    private int menuId;

    @Column(name = "ItemName")
    private String itemName;

    @Column(name = "ItemDescription")
    private String itemDescription;

    @Column(name = "ItemIcon")
    private String itemIcon;

    @Column(name = "ItemPrice")
    private long itemPrice;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurantId")
    private Restaurant restaurant;
}
