package com.example.fos.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "Orders")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OrderId")
    private int orderId;
    @Column(name = "OrderAmount")
    private long orderAmount;
    @Column(name = "RestaurantId")
    private int restaurantId;

    @OneToMany(mappedBy = "orderItemId", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<OrderItem> orderItems;

    @OneToOne
    @JoinColumn(name = "orderStatusId")
    private OrderStatus orderStatus;

    @Column(name = "OrderCreatedTime")
    private LocalDateTime orderCreatedTime;
}
