package com.example.fos.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "Restaurants")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RestaurantId")
    private int restaurantId;

    @Column(name = "RestaurantName")
    private String restaurantName;

    @Column(name = "RestaurantIcon")
    private String restaurantIcon;

    @OneToOne
    @JoinColumn(name = "locationId")
    private Location location;

    @Column(name = "Rating")
    private long rating;

    @Column(name = "RatingCount")
    private long ratingCount;

}
