package com.example.fos.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class MenuDto {
    private int menuId;
    private String itemName;
    private String itemDescription;
    private String itemIcon;
    private long itemPrice;
}
