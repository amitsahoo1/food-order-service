package com.example.fos.model.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class OrderDto {
    private int restaurantId;
    private List<MenuQuantityDto> menuId;
    private Long orderAmount;
}
