package com.example.fos.model.dto;

import com.example.fos.model.Menu;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RestaurantMenuDto {
    private int restaurantId;
    private String restaurantName;
    private boolean isDiscountAdded;
    private int discountPercentage;
    private List<MenuDto> menus;
}
