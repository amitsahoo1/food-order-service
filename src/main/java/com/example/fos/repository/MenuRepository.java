package com.example.fos.repository;

import com.example.fos.model.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MenuRepository extends JpaRepository<Menu, Integer> {

    @Query("FROM Menu m WHERE m.menuId in :menuIds")
    List<Menu> findByMenuId(@Param("menuIds") int[] menuIds);
}
