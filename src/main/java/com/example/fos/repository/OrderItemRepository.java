package com.example.fos.repository;

import com.example.fos.model.Order;
import com.example.fos.model.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderItemRepository extends JpaRepository<OrderItem, Integer> {
}
