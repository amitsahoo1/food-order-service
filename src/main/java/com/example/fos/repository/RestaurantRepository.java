package com.example.fos.repository;

import com.example.fos.model.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import java.util.List;

@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Integer> {

    @Query("FROM Restaurant r WHERE r.location.locationName = :location")
    List<Restaurant> findRestaurantsByLocation(@Param("location") String location);

    @Query(value = "Select r.restaurantId, r.restaurantName, m.menuId, m.itemName, m.itemDescription, m.itemIcon, m.itemPrice \n" +
            "\t, CASE WHEN DATEPART(HOUR, GETDATE()) BETWEEN 0 AND 6 THEN CONVERT(VARCHAR(2),Q1) \n" +
            "\t\tELSE CASE WHEN DATEPART(HOUR, GETDATE()) BETWEEN 7 AND 12 THEN CONVERT(VARCHAR(2),Q1) \n" +
            "\t\tELSE CASE WHEN DATEPART(HOUR, GETDATE()) BETWEEN 13 AND 18 THEN CONVERT(VARCHAR(2),Q1) \n" +
            "\t\tELSE CASE WHEN DATEPART(HOUR, GETDATE()) BETWEEN 19 AND 23 THEN CONVERT(VARCHAR(2),Q1)\n" +
            "\t\tEND END END END AS discountPercentage \n" +
            "            FROM Restaurants r JOIN Menus m on r.restaurantId = m.restaurantId\n" +
            "\t\t\tLEFT JOIN Discount d on d.RestaurantId = r.RestaurantId\n" +
            "            Where r.restaurantId = :restaurantId"
            , nativeQuery = true)
    List<Tuple> findRestaurantMenuByRestaurantId(@Param("restaurantId") int restaurantId);

}
