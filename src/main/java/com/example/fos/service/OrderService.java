package com.example.fos.service;

import com.example.fos.model.Order;
import com.example.fos.model.dto.OrderDto;
import org.springframework.stereotype.Service;

public interface OrderService {
    Order createOrder(OrderDto order);
}
