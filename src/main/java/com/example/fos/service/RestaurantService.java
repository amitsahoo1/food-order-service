package com.example.fos.service;

import com.example.fos.model.Restaurant;
import com.example.fos.model.dto.RestaurantMenuDto;
import org.springframework.stereotype.Service;

import java.util.List;

public interface RestaurantService {

    List<Restaurant> getRestaurantsByLocations(String location);

    RestaurantMenuDto getRestaurantMenu(int restaurantId);

}
