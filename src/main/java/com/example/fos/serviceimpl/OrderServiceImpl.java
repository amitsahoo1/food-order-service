package com.example.fos.serviceimpl;

import com.example.fos.constants.Status;
import com.example.fos.model.Menu;
import com.example.fos.model.Order;
import com.example.fos.model.OrderItem;
import com.example.fos.model.dto.MenuQuantityDto;
import com.example.fos.model.dto.OrderDto;
import com.example.fos.repository.MenuRepository;
import com.example.fos.repository.OrderItemRepository;
import com.example.fos.repository.OrderRepository;
import com.example.fos.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Override
    public Order createOrder(OrderDto order) {
        try {
            List<Menu> menus = menuRepository.findByMenuId(order.getMenuId().stream().mapToInt(MenuQuantityDto::getMenuId).toArray());
//        Long orderAmount = menus.stream().mapToLong(Menu::getItemPrice).sum();
//        if(orderAmount != order.getOrderAmount()) {
//            throw new RuntimeException("Order price does not match");
//        }
            List<OrderItem> items = new ArrayList<>();

            Order placeOrder = Order.builder()
                    .orderAmount(order.getOrderAmount())
                    .restaurantId(order.getRestaurantId())
                    .orderStatus(Status.ORDER_PLACED)
                    .orderCreatedTime(LocalDateTime.now())
                    .build();

            for (MenuQuantityDto menu : order.getMenuId()) {
                items.add(OrderItem.builder()
                        .itemId(menu.getMenuId())
                        .quantity(menu.getQuantity())
                        .order(placeOrder)
                        .build());
            }

            Order confirmedOrder = orderRepository.save(placeOrder);
            orderItemRepository.saveAll(items);
            confirmedOrder.setOrderItems(items);
            return confirmedOrder;
        } catch (Exception ex) {
            throw new RuntimeException("Error occurred while creating an order.");
        }
    }
}
