package com.example.fos.serviceimpl;

import com.example.fos.model.Restaurant;
import com.example.fos.model.dto.MenuDto;
import com.example.fos.model.dto.RestaurantMenuDto;
import com.example.fos.repository.RestaurantRepository;
import com.example.fos.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Override
    public List<Restaurant> getRestaurantsByLocations(String location) {
        List<Restaurant> result =  restaurantRepository.findRestaurantsByLocation(location);
        if(result.size() == 0 ) throw new RuntimeException("No restaurant is not present in " + location + " location");
        return result;
    }

    @Override
    public RestaurantMenuDto getRestaurantMenu(int restaurantId) {
        List<Tuple> result =  restaurantRepository.findRestaurantMenuByRestaurantId(restaurantId);
        if(result.size() == 0 ) throw new RuntimeException("Resturant is not present with Id:" + restaurantId);
        return extractRestaurantMenu(result);
    }

    public RestaurantMenuDto extractRestaurantMenu(List<Tuple> result) {
        RestaurantMenuDto restaurantMenuDto = new RestaurantMenuDto();
        restaurantMenuDto.setRestaurantId(result.get(0).get("restaurantId", Integer.class));
        restaurantMenuDto.setRestaurantName(result.get(0).get("restaurantName", String.class));

        final String discountString = result.get(0).get("discountPercentage", String.class);

        if(discountString == null || discountString.equalsIgnoreCase("0")) {
            restaurantMenuDto.setDiscountAdded(false);
            restaurantMenuDto.setDiscountPercentage(0);
        } else {
            restaurantMenuDto.setDiscountAdded(true);
            restaurantMenuDto.setDiscountPercentage(Integer.parseInt(discountString));
        }

        List<MenuDto> menus = new ArrayList<>();
        for(Tuple tuple: result) {
            menus.add(MenuDto.builder()
                    .menuId(tuple.get("menuId", Integer.class))
                    .itemName(tuple.get("itemName", String.class))
                    .itemDescription(tuple.get("itemDescription", String.class))
                    .itemIcon(tuple.get("itemIcon", String.class))
                    .itemPrice(tuple.get("itemPrice", BigDecimal.class).longValue())
                    .build());
        }
        restaurantMenuDto.setMenus(menus);
        return restaurantMenuDto;
    }
}
