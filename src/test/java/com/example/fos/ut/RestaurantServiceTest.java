package com.example.fos.ut;

import com.example.fos.model.Restaurant;
import com.example.fos.repository.RestaurantRepository;
import com.example.fos.service.RestaurantService;
import com.example.fos.serviceimpl.RestaurantServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
public class RestaurantServiceTest {

    @InjectMocks
    private RestaurantServiceImpl restaurantService;

    @Mock
    private RestaurantRepository restaurantRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindRestaurantByLocation() {
        List<Restaurant> lister = new ArrayList<>();
        lister.add(Restaurant.builder().restaurantName("test").build());
        when(restaurantRepository.findRestaurantsByLocation("bangalore")).thenReturn(lister);
        Assert.assertEquals(1, restaurantService.getRestaurantsByLocations("bangalore").size());
    }

}
